import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { FilmComponent } from './components/film/film.component';
import { FilmsComponent } from './components/films/films.component';
import { HomeComponent } from './components/home/home.component';
import { PeopleComponent } from './components/people/people.component';
import { PeoplesComponent } from './components/peoples/peoples.component';
import { PlanetComponent } from './components/planet/planet.component';
import { PlanetsComponent } from './components/planets/planets.component';
import { ShipComponent } from './components/ship/ship.component';
import { ShipsComponent } from './components/ships/ships.component';
import { SpecieComponent } from './components/specie/specie.component';
import { SpeciesComponent } from './components/species/species.component';
import { VehicleComponent } from './components/vehicle/vehicle.component';
import { VehiclesComponent } from './components/vehicles/vehicles.component';

const routes: Routes = [
  {path:'',component:HomeComponent},
  {path:'starships',component:ShipsComponent},
  {path:'starships/:id',component:ShipComponent},
  {path:'films',component:FilmsComponent},
  {path:'films/:id',component:FilmComponent},
  {path:'people',component:PeoplesComponent},
  {path:'people/:id',component:PeopleComponent},
  {path:'planets',component:PlanetsComponent},
  {path:'planets/:id',component:PlanetComponent},
  {path:'species',component:SpeciesComponent},
  {path:'species/:id',component:SpecieComponent},
  {path:'vehicles',component:VehiclesComponent},
  {path:'vehicles/:id',component:VehicleComponent},
  
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }

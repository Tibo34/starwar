import { DecimalPipe } from '@angular/common';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Planet } from '../models/Planet';
import { BaseService } from './base.service';

@Injectable({
  providedIn: 'root'
})
export class PlanetsService extends BaseService<Planet> {
  
  url_ships='planets';    

  constructor(public override http: HttpClient,public override pipe:DecimalPipe){ 
    super(http,pipe);   
    this.url_entitie+=this.url_ships;
    this.entitie=this.url_ships;
    this.getAll();
  }

 
}

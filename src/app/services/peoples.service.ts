import { DecimalPipe } from '@angular/common';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { People } from '../models/People';
import { BaseService } from './base.service';

@Injectable({
  providedIn: 'root'
})
export class PeoplesService extends BaseService<People> {
  url_ships='people';    

  constructor(public override http: HttpClient,public override pipe:DecimalPipe){ 
    super(http,pipe);   
    this.url_entitie+=this.url_ships;
    this.entitie=this.url_ships;
    this.getAll();
  }

}

import { DecimalPipe } from '@angular/common';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Specie } from '../models/Specie';
import { BaseService } from './base.service';

@Injectable({
  providedIn: 'root'
})
export class SpeciesService extends BaseService<Specie> {

  url_ships='species';    

  constructor(public override http: HttpClient,public override pipe:DecimalPipe){ 
    super(http,pipe);   
    this.url_entitie+=this.url_ships;
    this.entitie=this.url_ships;
    this.getAll();
  }

 
}

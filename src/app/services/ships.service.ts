import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { Observable, BehaviorSubject } from 'rxjs';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Ship } from '../models/ship';
import { Result } from '../models/result';
import { Fabric } from '../models/utils';
import { BaseService } from './base.service';
import { DecimalPipe } from '@angular/common';


const ShipDefault={
  id:0,
  name:'', 
  model:'',
  manufacturer:'',
  cost_in_credits:0,
  length:0,
  max_atmosphering_speed:0,
  crew:'',
  passengers:0,
  cargo_capacity:0,
  consumables:'',
  hyperdrive_rating:'',
  MGLT:0,
  starship_class:'',
  pilots:[],
  films:[],
  url:''
}

@Injectable({
  providedIn: 'root'
})
export class ShipsService extends BaseService<Ship> {

  url_ships='starships';  
  

  constructor(public override http: HttpClient,public override pipe: DecimalPipe){ 
    super(http,pipe);   
    this.url_entitie+=this.url_ships;
    this.entitie=this.url_ships;
    this.getAll();
  }

 
}

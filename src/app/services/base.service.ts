import { HttpClient } from '@angular/common/http';
import { Inject, Injectable, PipeTransform } from '@angular/core';
import { BehaviorSubject, debounceTime, delay, Observable, of, Subject, switchMap, tap } from 'rxjs';
import { environment } from 'src/environments/environment';
import { Result } from '../models/result';
import { Fabric } from '../models/utils';
import { EntitieBase } from "../models/entitieBase";
import { DecimalPipe } from '@angular/common';


interface State {
  page: number;
  pageSize: number; 
}


@Injectable({
  providedIn: 'root'
})
export class BaseService<EntitieBase> { 
  public _models:any[]=[];
  public _model:any={};
  public url_entitie=environment.url_api;
  public entitie:string='';

  private IdSelect:number=0;

  public Models=new BehaviorSubject<EntitieBase[]>(this._models);
  public Model=new BehaviorSubject<EntitieBase>(this._model);
  public properties=new BehaviorSubject<string[]>([]);
  public Total = new BehaviorSubject<number>(0);
  private _search$ = new Subject<void>();
  private _loading$ = new BehaviorSubject<boolean>(true);


  
  private _state: State = {
    page: 1,
    pageSize: 10,   
  };

  public get loading$() { return this._loading$.asObservable(); }
  public get page() { return this._state.page; }
  public get pageSize() { return this._state.pageSize; }


  set page(page: number) { this._set({page}); }
  set pageSize(pageSize: number) { this._set({pageSize}); }
  set IdSelect$(id:string){
    this.IdSelect=parseInt(id);    
    this._search$.next();
  }


  private _set(patch: Partial<State>) {
    Object.assign(this._state, patch);
    this._search$.next();
  }

  constructor(public http: HttpClient,public pipe: DecimalPipe) {      
    this._search$.pipe(
      tap(() => this._loading$.next(true)),
      debounceTime(200),
      switchMap(() => this._search()),
      delay(200),
      tap(() => this._loading$.next(false))
    ).subscribe(result => {     
      this.Models.next(result.models);
      this.Total.next(result.total);
      if(this.IdSelect!==0&&this._model?.id!==this.IdSelect){
        this._model=this._models.find((e:any)=>e.id==this.IdSelect);        
        this.Model.next(this._model);
      }
    });
    this._search$.next();   
  }


  loadData<T>(){
    return new Promise(async(resolve,reject)=>{
      this.LoadPage<T>(this.url_entitie);        
      resolve(this._models);     
    })
  }

  LoadPage<T>(url:string){
    this.http.get<Result>(url).subscribe((rep)=>{
      const models:any[]=rep.results.map(r=>{
        return Fabric<T>(r); 
      });
      this._models.push(...models);
      if(rep.next){
        this.LoadPage(rep.next);
      }     
      this._search$.next();
    });
  }

  public findById<EntitieBase>(id:number){
    const model=this._models.find(f=>f.id==id);
    return model;
  }  


  public GetUrlId(id:number){
    return this.entitie+'/'+id;
  }

  AddModel<T>(id:number){
    const model=this.findById<T>(id);
    if(model){
        this.emit(model);
      }
  }

  GetOneById<T>(id:number){
    this.IdSelect=id;
    if(this._models.length===0){
      this.loadData().then((elements)=>{        
        this.AddModel<T>(id);       
      });
    }else{
      this.AddModel<T>(id);
    }    
  }

  public getAll(){  
    this.loadData();     
  }

  public getById(id:number){     
     this.GetOneById(id);
  }

  public emits(){
    this.Models.next(this._models);
  }

  emit(item:any){
    this._model=item
    this.Model.next(this._model);
  }

  private _search(): Observable<any> {
    const {  pageSize, page} = this._state;
    const total = this._models.length;   
    let models = this._models.slice((page - 1) * pageSize, (page - 1) * pageSize + pageSize);
    return of({models, total});
  }
}

import { DecimalPipe } from '@angular/common';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { environment } from 'src/environments/environment';
import { Film } from '../models/film';
import { Result } from '../models/result';
import { Fabric } from '../models/utils';
import { BaseService } from './base.service';



@Injectable({
  providedIn: 'root'
})
export class FilmsService extends BaseService<Film>{

   url_ships='films';    

  constructor(public override http: HttpClient,public override pipe:DecimalPipe){ 
    super(http,pipe);   
    this.url_entitie+=this.url_ships;
    this.entitie=this.url_ships;
    this.getAll();
  }

 
}

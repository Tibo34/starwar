import { DecimalPipe } from '@angular/common';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Vehicle } from '../models/Vehicle';
import { BaseService } from './base.service';

@Injectable({
  providedIn: 'root'
})
export class VehiclesService extends BaseService<Vehicle> {
  url_ships='vehicles';    

  constructor(public override http: HttpClient,public override pipe:DecimalPipe){ 
    super(http,pipe);   
    this.url_entitie+=this.url_ships;
    this.entitie=this.url_ships;
    this.getAll();
  }

 
}

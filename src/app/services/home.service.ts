import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { Links } from '../models/Links';

@Injectable({
  providedIn: 'root'
})
export class HomeService {

  private _models=[];
  public Models=new BehaviorSubject<Links[]>(this._models);


  constructor(public http: HttpClient) { 
    this.getAll();
  }


  getAll(){
    this.http.get<string>('https://swapi.dev/api/').subscribe((rep:any)=>{
      const properties=Object.getOwnPropertyNames(rep);
      const links:Links[]=[];
      properties.forEach(p=>{
        const value=rep[p];
        links.push({name:p,url:value});
        this.Models.next(links);
      });
    })
  }
}

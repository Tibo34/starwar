import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { Links } from 'src/app/models/Links';
import { HomeService } from 'src/app/services/home.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {


  public links:Observable<Links[]>=new Observable();
  constructor(private HomeService:HomeService) { }

  ngOnInit(): void {
    this.links=this.HomeService.Models;
  }

}

import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Observable } from 'rxjs';
import { Film } from 'src/app/models/film';
import { FilmsService } from 'src/app/services/films.service';

@Component({
  selector: 'app-films',
  templateUrl: './films.component.html',
  styleUrls: ['./films.component.scss']
})
export class FilmsComponent implements OnInit {


  films:Observable<Film[]>=new Observable();
  total:Observable<number>=new Observable();

  constructor(public filmsService:FilmsService,private router: Router) { }

  ngOnInit(): void {
    this.films=this.filmsService.Models;
    this.total=this.filmsService.Total;
  }

  FindShip(id:number){   
    this.router.navigate([this.filmsService.GetUrlId(id)]);
  }

}

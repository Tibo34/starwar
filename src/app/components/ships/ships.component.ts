import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Observable } from 'rxjs';
import { Ship } from 'src/app/models/ship';
import { ShipsService } from 'src/app/services/ships.service';

@Component({
  selector: 'app-ships',
  templateUrl: './ships.component.html',
  styleUrls: ['./ships.component.scss']
})
export class ShipsComponent implements OnInit {

  Ships:Observable<Ship[]>=new Observable();
  total:Observable<number>=new Observable();

  constructor(public shipsService:ShipsService,private router: Router) { }

  ngOnInit(): void {
    this.Ships=this.shipsService.Models;
    this.total=this.shipsService.Total;
  }

  FindShip(id:number){   
    this.router.navigate([this.shipsService.GetUrlId(id)]);
  }

}

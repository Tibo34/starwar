import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { Planet } from 'src/app/models/Planet';
import { PlanetsService } from 'src/app/services/planets.service';

@Component({
  selector: 'app-planet',
  templateUrl: './planet.component.html',
  styleUrls: ['./planet.component.scss']
})
export class PlanetComponent implements OnInit {

  public Planet:Observable<Planet>=new Observable();

  constructor(private VehiclesService:PlanetsService,private route: ActivatedRoute) { }

  ngOnInit(): void {
    const id=this.route.snapshot.paramMap.get('id');
    if(id)
    {
     const idInt=parseInt(id);
     this.VehiclesService.getById(idInt);
    }        
    this.Planet=this.VehiclesService.Model;
  }

}

import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { Vehicle } from 'src/app/models/Vehicle';
import { VehiclesService } from 'src/app/services/vehicles.service';

@Component({
  selector: 'app-vehicle',
  templateUrl: './vehicle.component.html',
  styleUrls: ['./vehicle.component.scss']
})
export class VehicleComponent implements OnInit {

  public Vehicle:Observable<Vehicle>=new Observable();

  constructor(private VehiclesService:VehiclesService,private route: ActivatedRoute) { }

  ngOnInit(): void {
    const id=this.route.snapshot.paramMap.get('id');
    if(id)
    {
     const idInt=parseInt(id);
     this.VehiclesService.getById(idInt);
    }        
    this.Vehicle=this.VehiclesService.Model;
  }
}

import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Observable } from 'rxjs';
import { People } from 'src/app/models/People';
import { PeoplesService } from 'src/app/services/peoples.service';


@Component({
  selector: 'app-peoples',
  templateUrl: './peoples.component.html',
  styleUrls: ['./peoples.component.scss']
})
export class PeoplesComponent implements OnInit {

  Peoples:Observable<People[]>=new Observable();
  total:Observable<number>=new Observable();

  constructor(public peopleService:PeoplesService,private router: Router) { }

  ngOnInit(): void {
    this.Peoples=this.peopleService.Models;
    this.total=this.peopleService.Total;
  }

  FindShip(id:number){   
    this.router.navigate([this.peopleService.GetUrlId(id)]);
  }

}

import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Observable } from 'rxjs';
import { Planet } from 'src/app/models/Planet';
import { PlanetsService } from 'src/app/services/planets.service';

@Component({
  selector: 'app-planets',
  templateUrl: './planets.component.html',
  styleUrls: ['./planets.component.scss']
})
export class PlanetsComponent implements OnInit {

  Planets:Observable<Planet[]>=new Observable();
  total:Observable<number>=new Observable();

  constructor(public planetsService:PlanetsService,private router: Router) { }

  ngOnInit(): void {
    this.Planets=this.planetsService.Models;
    this.total=this.planetsService.Total;
  }

  FindShip(id:number){   
    this.router.navigate([this.planetsService.GetUrlId(id)]);
  }

}

import { Component, Input, OnInit } from '@angular/core';
import { BaseService } from 'src/app/services/base.service';

@Component({
  selector: 'app-pagination',
  templateUrl: './pagination.component.html',
  styleUrls: ['./pagination.component.scss']
})
export class PaginationComponent implements OnInit {

  @Input()
  total:number|null=0;

  @Input()
  service:any;

  constructor() { }

  ngOnInit(): void {
  }

}

import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Observable } from 'rxjs';
import { Vehicle } from 'src/app/models/Vehicle';
import { VehiclesService } from 'src/app/services/vehicles.service';


@Component({
  selector: 'app-vehicles',
  templateUrl: './vehicles.component.html',
  styleUrls: ['./vehicles.component.scss']
})
export class VehiclesComponent implements OnInit {

  Vehicles:Observable<Vehicle[]>=new Observable();
  total:Observable<number>=new Observable();

  constructor(public vehicleService:VehiclesService,private router: Router) { }

  ngOnInit(): void {
    this.Vehicles=this.vehicleService.Models;
    this.total=this.vehicleService.Total;
  }

  FindShip(id:number){   
    this.router.navigate([this.vehicleService.GetUrlId(id)]);
  }

}

import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { People } from 'src/app/models/People';
import { PeoplesService } from 'src/app/services/peoples.service';

@Component({
  selector: 'app-people',
  templateUrl: './people.component.html',
  styleUrls: ['./people.component.scss']
})
export class PeopleComponent implements OnInit {

 
  public People:Observable<People>=new Observable();

  constructor(private peopleService:PeoplesService,private route: ActivatedRoute) { }

  ngOnInit(): void {
    const id=this.route.snapshot.paramMap.get('id');    
    this.People=this.peopleService.Model;
    if(id)
    {        
     this.peopleService.IdSelect$=id;    
    }        
   
  }

}

import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { Ship } from 'src/app/models/ship';
import { ShipsService } from 'src/app/services/ships.service';
import {Location} from '@angular/common';

@Component({
  selector: 'app-ship',
  templateUrl: './ship.component.html',
  styleUrls: ['./ship.component.scss']
})
export class ShipComponent implements OnInit {

  public Ship:Observable<Ship>=new Observable();

  constructor(private shipService:ShipsService,private route: ActivatedRoute,private location:Location) { }

  ngOnInit(): void {
    const id=this.route.snapshot.paramMap.get('id');  
    this.Ship=this.shipService.Model;
    if(id)
    {
      this.shipService.IdSelect$=id;
    }    
  }

  moveTo(entitie:string,id:any){        
    this.location.go(entitie+'/'+id);
  }

}

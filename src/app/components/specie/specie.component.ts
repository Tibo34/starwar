import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { Specie } from 'src/app/models/Specie';
import { SpeciesService } from 'src/app/services/species.service';

@Component({
  selector: 'app-specie',
  templateUrl: './specie.component.html',
  styleUrls: ['./specie.component.scss']
})
export class SpecieComponent implements OnInit {

  public Specie:Observable<Specie>=new Observable();

  constructor(private SpeciesService:SpeciesService,private route: ActivatedRoute) { }

  ngOnInit(): void {
    const id=this.route.snapshot.paramMap.get('id');
    if(id)
    {
     const idInt=parseInt(id);
     this.SpeciesService.getById(idInt);
    }        
    this.Specie=this.SpeciesService.Model;
  }

}

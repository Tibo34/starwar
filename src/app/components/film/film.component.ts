import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { Film } from 'src/app/models/film';
import { FilmsService } from 'src/app/services/films.service';


@Component({
  selector: 'app-film',
  templateUrl: './film.component.html',
  styleUrls: ['./film.component.scss']
})
export class FilmComponent implements OnInit {

  public Film:Observable<Film>=new Observable();

  constructor(private filmService:FilmsService,private route: ActivatedRoute) { }

  ngOnInit(): void {
    const id=this.route.snapshot.paramMap.get('id');
    if(id)
    {
     const idInt=parseInt(id);
     this.filmService.getById(idInt);
    }        
    this.Film=this.filmService.Model;
  }

}

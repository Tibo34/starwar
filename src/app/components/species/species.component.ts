import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Observable } from 'rxjs';
import { Specie } from 'src/app/models/Specie';
import { SpeciesService } from 'src/app/services/species.service';

@Component({
  selector: 'app-species',
  templateUrl: './species.component.html',
  styleUrls: ['./species.component.scss']
})
export class SpeciesComponent implements OnInit {

  Species:Observable<Specie[]>=new Observable();
  total:Observable<number>=new Observable();

  constructor(public speciesService:SpeciesService,private router: Router) { }

  ngOnInit(): void {
    this.Species=this.speciesService.Models;
    this.total=this.speciesService.Total;
  }

  FindShip(id:number){   
    this.router.navigate([this.speciesService.GetUrlId(id)]);
  }

}

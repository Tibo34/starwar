import { EntitieBase } from "./entitieBase";


export interface Vehicle extends EntitieBase{ 
    cargo_capacity:string,
    consumables:string,
    cost_in_credits:number,
    crew:number,
    length:number,
    manufacturer:string,
    max_atmosphering_speed:number,
    model:string,
    name:string,
    passengers:number,
    pilots: [],
    films: [],
    url:string, 
    vehicle_class:string,
}
import { EntitieBase } from "./entitieBase";


export interface Specie extends EntitieBase{
    average_height:string,
    average_lifespan:string,
    classification:string,
    designation:string,
    eye_colors:string,
    hair_colors:string,
    homeworld:string,
    language:string,
    name:string,
    people: [],
    films: [],
    skin_colors:string,
    url:string,
}
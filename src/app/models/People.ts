import { EntitieBase } from "./entitieBase";


export interface People extends EntitieBase {
    birth_year:string
    eye_color:string,
    films:[]
    gender:string,
    hair_color:string,
    height:string,
    homeworld:string
    mass:number,
    name:string,
    skin_color:string,
    species:[],
    starships:[],
    url: string,
    vehicles:[]
}
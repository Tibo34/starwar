import { EntitieBase } from "./entitieBase";
import { Film } from "./film";
import { Pilot } from "./pilot";


export interface Ship extends EntitieBase {
    name:string, 
    model:string,
    manufacturer:string,
    cost_in_credits:number,
    length:number,
    max_atmosphering_speed:number,
    crew:string,
    passengers:number,
    cargo_capacity:number,
    consumables:string,
    hyperdrive_rating:string,
    MGLT:number,
    starship_class:string,
    pilots:Pilot[],
    films:Film[],  
}




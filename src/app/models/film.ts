import { EntitieBase } from "./entitieBase";
import { People } from "./People";
import { Planet } from "./Planet";

export interface Film extends EntitieBase{
    title: string,
    episode_id: number,
    opening_crawl: string,
    director:string,
    producer:string,
    release_date: Date,
    characters: People[],
    planets:Planet[],    
}

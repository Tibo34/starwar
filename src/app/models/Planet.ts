import { EntitieBase } from "./entitieBase";

export interface Planet extends EntitieBase{
    climate:string,
    diameter: string,
    films:[],
    gravity: number,
    name: string,
    orbital_period: number,
    population: number,
    residents:[],
    rotation_period:number,
    surface_water: number,
    terrain: string,
    url: string
}
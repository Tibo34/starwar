import { EntitieBase } from "./entitieBase";


const entities=['films','people','planets','species','starships','vehicles',];
// "https://swapi.dev/api/films/1/"
export function ExtractId(url:string):number{
    const split=url.split('/');
    const id=split[split.length-2];
    return parseInt(id);
}

export function Fabric<T>(data:any):EntitieBase{
    const properties=Object.getOwnPropertyNames(data);
    const entitiesPropreties=properties.filter(p=>{
        return typeof data[p]==='object';         
    });
    entitiesPropreties.forEach(element => {
        const list=data[element]?.map((e:any)=>ExtractId(e));
        data[element]=list;
    });
    const model:EntitieBase=data;    
    model.id=ExtractId(model.url);       
    return model;
}



import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { HttpClientModule } from '@angular/common/http';
import { AppRoutingModule } from './app-routing.module';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { AppComponent } from './app.component';
import { HeaderComponent } from './components/header/header.component';
import { HomeComponent } from './components/home/home.component';
import { ShipsComponent } from './components/ships/ships.component';
import { ShipComponent } from './components/ship/ship.component';
import { FilmsComponent } from './components/films/films.component';
import { FilmComponent } from './components/film/film.component';
import { PlanetsComponent } from './components/planets/planets.component';
import { PlanetComponent } from './components/planet/planet.component';
import { SpeciesComponent } from './components/species/species.component';
import { SpecieComponent } from './components/specie/specie.component';
import { VehiclesComponent } from './components/vehicles/vehicles.component';
import { VehicleComponent } from './components/vehicle/vehicle.component';
import { PeoplesComponent } from './components/peoples/peoples.component';
import { PeopleComponent } from './components/people/people.component';
import { DecimalPipe } from '@angular/common';
import { PaginationComponent } from './components/pagination/pagination.component';
import { SpeciesService } from './services/species.service';
import { PeoplesService } from './services/peoples.service';
import { PlanetsService } from './services/planets.service';
import { VehiclesService } from './services/vehicles.service';
import { FilmsService } from './services/films.service';
import { ShipsService } from './services/ships.service';


@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    HomeComponent,
    ShipsComponent,
    ShipComponent,
    FilmsComponent,
    FilmComponent,
    PlanetsComponent,
    PlanetComponent,
    SpeciesComponent,
    SpecieComponent,
    VehiclesComponent,
    VehicleComponent,
    PeoplesComponent,
    PeopleComponent,    
    PaginationComponent,    
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    ReactiveFormsModule,
    NgbModule,
    FormsModule
  ],
  providers: [DecimalPipe,SpeciesService,PeoplesService,PlanetsService,VehiclesService,FilmsService,ShipsService],
  bootstrap: [AppComponent]
})
export class AppModule { 
  constructor(private spece:SpeciesService,private people:PeoplesService,private planet:PlanetsService,private vehicle:VehiclesService,private film:FilmsService,private ships:ShipsService){

  }
}
